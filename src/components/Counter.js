import { counterActions } from "../store";
import { useSelector, useDispatch } from "react-redux";
import classes from "./Counter.module.css";

const Counter = () => {
  const dispatch = useDispatch();
  const counter = useSelector((state) => state.counter);
  const show = useSelector((state) => state.showCounter);
  // useSelector = (state) => state.showCounter;
  const incrementCounter = () => {
    dispatch(counterActions.INCREMENT());
  };
  const userInput = () => {
    dispatch(counterActions.INPUT_BY_USER(10));
  };
  const decrementCounter = () => {
    dispatch(counterActions.DECREMENT());
  };

  const togggleCounter = () => {
    dispatch(counterActions.TOGGLE_COUNTER());
  };

  // const handleDelete = () => {
  //   dispatch(counterActions.INCREMENT());
  // };

  return (
    <main className={classes.counter}>
      <h1>Redux Counter</h1>
      {show && <div className={classes.value}>{counter}</div>}
      <button onClick={incrementCounter}>IINCREMENT</button>
      <button onClick={userInput}>USER INPUT By 10</button>
      <button onClick={decrementCounter}>DECREMENT</button>
      {/* <button onClick={handleDelete}>RISET</button> */}
      <button onClick={togggleCounter}>TOGGLE COUNTER</button>
    </main>
  );
};

export default Counter;
