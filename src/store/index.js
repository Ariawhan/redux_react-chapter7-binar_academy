import { composeWithDevTools } from "redux-devtools-extension";

import { createSlice, configureStore } from "@reduxjs/toolkit";

const initialState = {
  counter: localStorage.getItem("Counter") || 0,
  showCounter: true,
};

const counterSlice = createSlice({
  name: "counter",
  initialState: initialState,
  reducers: {
    INCREMENT(state) {
      state.counter++;
    },
    INPUT_BY_USER(state, action) {
      state.counter += action.payload;
    },
    DECREMENT(state) {
      state.counter--;
    },
    TOGGLE_COUNTER(state) {
      showCounter = !state.showCounter;
    },
  },
});

const counterReducer = (state = initialState, action) => {
  switch (action.type) {
    case "INCREMENT":
      localStorage.setItem("Counter", state.counter + 1);
      return {
        counter: state.counter + 1,
        showCounter: state.showCounter,
      };
    case "INPUT_BY_USER":
      localStorage.setItem("Counter", state.counter + action.value);
      return {
        counter: state.counter + state.counter + action.value,
        showCounter: state.showCounter,
      };
    case "DECREMENT":
      localStorage.setItem("Counter", state.counter - 1);
      return {
        counter: state.counter - 1,
        showCounter: state.showCounter,
      };
    case "TOGGLE_COUNTER":
      return {
        showCounter: !state.showCounter,
        counter: state.counter,
      };
    case "HANDLE_DELETE":
      localStorage.setItem("Counter", 0);
      return {
        counter: 0,
        showCounter: state.showCounter,
      };
    default:
      return state;
  }
};

const store = configureStore({ counter: { counter: counterSlice.reducers } });

export const counterActions = 
export default store;
